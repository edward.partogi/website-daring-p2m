from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.ProdukPelatihan)
admin.site.register(models.Program)
admin.site.register(models.Pelatihan)