from django.urls import path

from . import views

urlpatterns = [
    path("list", views.program_list, name="program-list"),
    # path("get-auth", views.signup_user, name="authenticate"),
]
