from django.db import models

# Create your models here.

# IMPORTANT FOR A TEMPLATE ISSUE POSSIBILITY !!
# Don't forget to update 'templatetags/val_config.py' if there are any new unique char at these models' name.

class ProdukPelatihan(models.Model):
    id_produk = models.AutoField(primary_key=True)
    nama = models.TextField()
    
    def __str__(self):
        return self.nama

    class Meta:
        db_table = "produk_pelatihan"

class Program(models.Model):
    id_program = models.AutoField(primary_key=True)
    jenis_prod_pelatihan = models.ForeignKey(ProdukPelatihan, on_delete=models.CASCADE, db_column = 'id_produk')
    nama = models.TextField()
    deskripsi = models.TextField(blank=True)

    def __str__(self):
        return self.nama

    class Meta:
        db_table = "program"

class Pelatihan(models.Model):
    id_program = models.ForeignKey(Program, on_delete=models.CASCADE, db_column = 'id_program')
    # nama atribut pelatihan-pelatihan di entiti Program adalah pelatihan_set
    id_pelatihan = models.AutoField(primary_key=True)

    judul = models.TextField()
    deskripsi = models.TextField(blank=True)
    jumlah_hari = models.PositiveSmallIntegerField(blank=True, null=True)
    biaya = models.PositiveIntegerField(blank=True, null=True)
    detail_url = models.URLField(max_length=250)

    def __str__(self):
        return self.judul

    class Meta:
        db_table = "pelatihan"