from django.shortcuts import render
from .models import ProdukPelatihan as Produk, Program, Pelatihan
from user.views import base_context

# Create your views here.
def program_list(request):
    context = base_context(request)
    
    jenis_prod = Produk.objects.all()

    produk_set = {}
    for produk in jenis_prod:
        produk_str = produk.__str__()
        program_in_produk = Program.objects.filter(jenis_prod_pelatihan=produk.id_produk)

        program_set = {}
        for program in program_in_produk:
            program_str = program.__str__()
            pelatihan_in_program = Pelatihan.objects.filter(id_program=program.id_program)
            program_set[program_str] = pelatihan_in_program

        produk_set[produk_str] = program_set

    context['all_produk'] = produk_set

    return render(request, "daftar_program.html", context)