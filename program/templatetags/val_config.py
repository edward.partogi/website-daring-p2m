from django import template

register = template.Library()

# To change values from views.py to have a proper name to be used as id in HTML codes
@register.filter
def html_id(value):
    return value.replace(" ", "_")\
    .replace("&", "n")\
    .replace("(", "or_")\
    .replace(")", "")

@register.filter
def get_kelas_aktif(pelatihan):
    kelas_aktif = pelatihan.kelas_set.filter(aktif='True')
    if kelas_aktif.count() == 0:
        return "Tidak aktif"
    else:
        return "Aktif"

@register.filter
def get_kelas(pelatihan):
    kelas_aktif = pelatihan.kelas_set.filter(aktif='True')
    if kelas_aktif.count() > 0:
        return pelatihan.kelas_set.filter(aktif='True')[0].id_kelas
    else:
        return None