This folder is made to override templates from django-allauth library.
The directories are made exactly like django-allauth's templates directories so when django-allauth's view calls the templates, it will call these templates instead.

Note : The directories of django-allauth library can change over time due to its updates, so it may cause these templates to not working. Please keep checking on the updates.