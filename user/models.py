from django.db import models

# Create your models here.
class Peserta(models.Model):
    id = models.AutoField(primary_key=True)
    nama_lengkap = models.TextField()
    email = models.EmailField()
    no_hp = models.CharField(max_length=15, blank=True)
    tempat_lahir = models.CharField(max_length=50, blank=True)
    tanggal_lahir = models.DateField(null=True, blank=True)
    alamat_rumah = models.CharField(max_length=300, blank=True)
    no_telepon_rumah = models.CharField(max_length=15, blank=True)

    def __str__(self):
        return self.nama_lengkap

    class Meta:
        db_table = "peserta"

class Instruktur(models.Model):
    id = models.AutoField(primary_key=True)
    nama_lengkap = models.TextField()
    email = models.EmailField()
    no_hp = models.CharField(max_length=15)

    def __str__(self):
        return self.nama_lengkap

    class Meta:
        db_table = "instruktur"