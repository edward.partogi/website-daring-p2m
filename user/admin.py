from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Peserta)
admin.site.register(models.Instruktur)
