from django.shortcuts import render, redirect
from .models import Peserta, Instruktur
from django.db.models.base import ObjectDoesNotExist
from django.http import Http404

# Create your views here.
def get_pesertaObj(request):
    user_email = request.session.get('peserta_email')
    user_id = request.session.get('peserta_id')
    peserta = Peserta.objects.get(email=user_email, id=user_id)
    
    return peserta

def get_kelasPeserta(request):
    peserta = get_pesertaObj(request)
    kelas_set = peserta.kelas_set.filter(aktif=True)
    return kelas_set

def base_context(request):
    peserta_context = {}
    if active_user(request):
        peserta_context['peserta_obj'] = get_pesertaObj(request)
        peserta_context['kelas_peserta'] = get_kelasPeserta(request)
    return peserta_context

def home(request):
    context = base_context(request)    
    return render(request, "dashboard.html", context)

def profile(request):
    peserta_context = {}
    if active_user(request):
        peserta_context['peserta_obj'] = get_pesertaObj(request)
        peserta_context['kelas_peserta'] = get_kelasPeserta(request)
        return render(request, "profile.html", peserta_context)
    else:
        raise Http404("URL not called correctly")

def active_user(request):
    if request.user.is_staff:
        raise Http404("Please log out from admin server first")
    elif request.user.is_authenticated:
        try:
            return request.session['peserta_email'] == request.user.email
        except KeyError:
            return False

def signup_user(request):
    if request.user.is_authenticated:
        peserta = None
        try:
            user_email = request.user.email
            peserta = Peserta.objects.get(email=user_email)
        except Peserta.DoesNotExist:
            user_name = request.user.first_name + " " + request.user.last_name
            peserta = Peserta(nama_lengkap=user_name, email=user_email)
            peserta.save()
        
        request.session['peserta_id'] = peserta.id
        request.session['peserta_email'] = peserta.email
        return redirect('/')
    else:
        raise Http404("URL not called correctly")

def restrict_url(request):
    raise Http404("URL not called correctly")