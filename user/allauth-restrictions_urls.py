from django.urls import path, re_path

from . import views

urlpatterns = [
    path("signup/", views.restrict_url, name="account_signup"),
    path("login/", views.restrict_url, name="account_login"),
    # path("logout/", views.logout, name="account_logout"), # this path is still used
    path("password/change/", views.restrict_url,
         name="account_change_password"),
    path("password/set/", views.restrict_url, name="account_set_password"),
    path("inactive/", views.restrict_url, name="account_inactive"),

    # E-mail
    path("email/", views.restrict_url, name="account_email"),
    path("confirm-email/", views.restrict_url,
         name="account_email_verification_sent"),
    re_path(r"^confirm-email/(?P<key>[-:\w]+)/$", views.restrict_url,
            name="account_confirm_email"),

    # password reset
    path("password/reset/", views.restrict_url,
         name="account_reset_password"),
    path("password/reset/done/", views.restrict_url,
         name="account_reset_password_done"),
    re_path(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$",
            views.restrict_url,
            name="account_reset_password_from_key"),
    path("password/reset/key/done/", views.restrict_url,
         name="account_reset_password_from_key_done"),
]