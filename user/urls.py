from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("profile", views.profile, name="profile_detail"),
    path("get-auth", views.signup_user, name="authenticate"),
]
