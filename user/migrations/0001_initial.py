# Generated by Django 3.1 on 2020-08-10 11:10

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Instruktur',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nama_lengkap', models.TextField()),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Peserta',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nama_lengkap', models.TextField()),
                ('email', models.EmailField(max_length=254)),
                ('no_hp', models.CharField(max_length=15)),
                ('tempat_lahir', models.CharField(max_length=50)),
                ('tanggal_lahir', models.DateField()),
                ('alamat_rumah', models.CharField(max_length=150)),
                ('no_telepon_rumah', models.CharField(blank=True, max_length=15)),
            ],
        ),
    ]
