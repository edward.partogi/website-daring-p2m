from django.db import models
from user.models import *
from program.models import Pelatihan

# Create your models here.
class Kelas(models.Model):
    id_kelas = models.AutoField(primary_key=True)
    # nama atribut kelas di entiti Pelatihan adalah kelas_set
    id_pelatihan = models.ForeignKey(Pelatihan, on_delete=models.CASCADE, db_column='id_pelatihan')
    id_instruktur = models.ForeignKey(Instruktur, on_delete=models.CASCADE, db_column='id_instruktur')
    id_peserta = models.ManyToManyField(Peserta, through='Transaksi')

    tanggal_mulai = models.DateField()
    tanggal_selesai = models.DateField()

    aktif = models.BooleanField(default=False, help_text='Penanda bahwa peserta sudah bisa mulai mendaftar kelas ini')

    def __str__(self):
        return str(self.id_kelas)+"-"+str(self.tanggal_mulai)+" "+self.id_pelatihan.__str__()

    class Meta:
        db_table = "kelas"

class Transaksi(models.Model):
    id_transaksi = models.AutoField(primary_key=True)
    id_peserta = models.ForeignKey(Peserta, on_delete=models.CASCADE, db_column='id_peserta')
    id_kelas = models.ForeignKey(Kelas, on_delete=models.CASCADE, db_column='id_kelas')
    bukti_transaksi = models.ImageField(upload_to='user/bt_tra/', blank=True)
    terkonfirmasi = models.BooleanField(default=False)
    
    def __str__(self):
        return str(self.id_transaksi)+"_"+self.id_peserta.__str__()+" "+self.id_kelas.__str__()


class Bab_Pelatihan(models.Model):
    id_bab = models.AutoField(primary_key=True)
    # nama atribut bab_pelatihan di entiti Kelas adalah bab_pelatihan_set
    id_kelas = models.ForeignKey(Kelas, on_delete=models.CASCADE, db_column='id_kelas')
    judul_bab = models.TextField()

    class Meta:
        db_table = "bab_pelatihan"

    def __str__(self):
        return str(self.id_kelas)+"-"+self.judul_bab

class Utas(models.Model):
    id_utas = models.AutoField(primary_key=True)
    # nama atribut utas di entiti Kelas adalah utas_set
    id_kelas = models.ForeignKey(Kelas, on_delete=models.CASCADE, db_column='id_kelas')

    judul_utas = models.TextField()
    #post_awal = models.OneToOneField(Post, on_delete=models.CASCADE)
    
    PESERTA = 'PS'
    INSTRUKTUR = "IR"
    ROLES_CHOICES = [(PESERTA, "Peserta"), (INSTRUKTUR, "Instruktur")]
    role_pembuat_utas = models.CharField(max_length=2, choices=ROLES_CHOICES)
    id_pembuat_utas = models.CharField(max_length=2000)

    class Meta:
        db_table = "utas"

class Post(models.Model):
    id_post = models.AutoField(primary_key=True)
    # nama atribut post di entiti Utas adalah post_set
    id_utas = models.ForeignKey(Utas, on_delete=models.CASCADE, db_column='id_utas')
    id_post_terbalas = models.ForeignKey('self', on_delete=models.CASCADE, db_column='id_post_terbalas', blank=True, null=True)

    PESERTA = 'PS'
    INSTRUKTUR = "IR"
    ROLES_CHOICES = [(PESERTA, "Peserta"), (INSTRUKTUR, "Instruktur")]
    role_pembuat_post = models.CharField(max_length=2, choices=ROLES_CHOICES)
    id_pembuat_post = models.CharField(max_length=2000)

    isi = models.TextField()
    waktu_dibuat = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "post"