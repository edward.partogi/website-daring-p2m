from django.urls import path

from . import views

urlpatterns = [
    path("pelatihan-register", views.daftar_pelatihan, name="daftar-pelatihan"),
    path("<int:id_kelas>/", views.goTo_kelas, name="kelas"),
]
