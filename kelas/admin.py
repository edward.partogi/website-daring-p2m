from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Kelas)
admin.site.register(models.Transaksi)
admin.site.register(models.Utas)
admin.site.register(models.Post)
admin.site.register(models.Bab_Pelatihan)