from django.shortcuts import render
from django.http import HttpResponseRedirect, Http404
from .models import *
from .forms import ImageForm
from user.models import Peserta
from user.views import base_context, get_pesertaObj

# Create your views here.
def daftar_pelatihan(request):
    if request.method == 'POST':
        peserta = Peserta.objects.get(id=request.POST.get('id_peserta'))
        kelas = Kelas.objects.get(id_kelas=request.POST.get('id_kelas'))
        new_transaksi = Transaksi(
            id_peserta=peserta,
            id_kelas=kelas)
        new_transaksi.save()
        return HttpResponseRedirect('/')
    raise Http404("URL not called correctly")

def goTo_kelas(request, id_kelas):
    context = base_context(request)
    said_kelas = Kelas.objects.get(id_kelas=id_kelas)
    said_transaksi = Transaksi.objects.get(
        id_peserta=get_pesertaObj(request),
        id_kelas=id_kelas)
    
    if request.method == 'POST':
        form = ImageForm(request.POST)
        if form.is_valid():
            said_transaksi.bukti_transaksi = form.cleaned_data['imageFile']
            said_transaksi.save()
    elif request.method == 'GET':
        context['img_form'] = ImageForm()
    
    context['curr_kelas'] = said_kelas
    context['transaksi'] = said_transaksi
    
    return render(request, "kelas.html", context)